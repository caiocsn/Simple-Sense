
#include "simple_sense.h"

void my_callback3(const char* key_value){
    if(strcmp(key_value,"on") == 0)
        gpio_write(5, 1);
    else if(strcmp(key_value,"off") == 0)
        gpio_write(5,0);

}

void user_init(void) {
    uart_set_baud(0, 115200);

    sensor_init("test_sensor1",2,NULL);

    //gpio_enable(5, GPIO_OUTPUT);
    //gpio_write(5,0);
    //actuator_init("red_led", my_callback3);
    xTaskCreate(&wifi_task, "wifi_task", 256, NULL, 2, NULL);
    xTaskCreate(&sync, "sync", 256, NULL, 2, NULL);
    xTaskCreate(&simple_sense, "simple_sense", 2048, NULL, 2, NULL);

    simple_sense_controller(simple_sense_run);

}
