// AWS IoT client endpoint
const char *client_endpoint = "a1hcr4tkx1rgc5.iot.us-east-2.amazonaws.com";

// AWS IoT device certificate (ECC)
const char *client_cert =
"-----BEGIN CERTIFICATE-----\r\n"
"MIIC7jCCAdagAwIBAgIUV7fgGqg0ENET/Zr7QwbreR3iMb0wDQYJKoZIhvcNAQEL\r\n"
"BQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g\r\n"
"SW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTE3MTExMjIyMTAz\r\n"
"NVoXDTQ5MTIzMTIzNTk1OVowfjELMAkGA1UEBhMCVVMxDTALBgNVBAgMBE9oaW8x\r\n"
"ETAPBgNVBAcMCENvbHVtYnVzMQwwCgYDVQQKDANBV1MxDDAKBgNVBAsMA0lPVDEN\r\n"
"MAsGA1UEAwwEQ2FpbzEiMCAGCSqGSIb3DQEJARYTY2Fpb2NzbkBob3RtYWlsLmNv\r\n"
"bTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABKsVsI4BleeC/B27Tph/51Hje5QD\r\n"
"fO6/hx41lDX7GEif2Pj8WulifuXsUUnPMyCd6EubH2F+Chder6530jFW8NKjYDBe\r\n"
"MB8GA1UdIwQYMBaAFAbUPevVJBpMTykqU6FIElzavdUSMB0GA1UdDgQWBBQP4X2d\r\n"
"TWq/ZTKX7tPjO/hcuEgHnjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIHgDAN\r\n"
"BgkqhkiG9w0BAQsFAAOCAQEAcjydoA7Sc69acA4xqAE5/9fvnyiOJ0z3NHgFu/VR\r\n"
"qrpXB0Aba/vJsKk5TlWMiah7r/0uspNws6r5Q4ViUO3AS6ocKRG58Yz9rhOXkWSC\r\n"
"IIv/njK8HhZIxlYunp6o8A/nQKDY4uBXLIaMFStRvyGYTLN/vtfvmBkzK7q4Savp\r\n"
"Q+x5DEBPNOpMBMi06y1IJBuICbaaQotnNg03jRwPXa6R0B91uaFuQY1/nA97ryM0\r\n"
"qYsv87zQ5d98TsaO1BiAO6lgV7n82JiNkNOPfhJ/TUFKtYS5RtmCbddD1soD1x/+\r\n"
"NuC6y/DTFMGVe1bmPjTpahS2EcWFbCYV+U6ymTErdMM1Eg==\r\n"
"-----END CERTIFICATE-----\r\n";

// AWS IoT device private key (ECC)
const char *client_key = 
"-----BEGIN EC PARAMETERS-----\r\n"
"BggqhkjOPQMBBw==\r\n"
"-----END EC PARAMETERS-----\r\n"
"-----BEGIN EC PRIVATE KEY-----\r\n"
"MHcCAQEEIAFYycHZwi4PxQ2zFnt3b1TsVrYLkhnocVWs2USxwvIDoAoGCCqGSM49\r\n"
"AwEHoUQDQgAEqxWwjgGV54L8HbtOmH/nUeN7lAN87r+HHjWUNfsYSJ/Y+Pxa6WJ+\r\n"
"5exRSc8zIJ3oS5sfYX4KF16vrnfSMVbw0g==\r\n"
"-----END EC PRIVATE KEY-----\r\n";