#include "simple_sense.h"

typedef struct{
    char * name;
    uint8_t port;
    int (*callback)(uint8_t);
}Sensor;

typedef struct{
    char name[20];
    uint8_t name_len;
    void (*callback)(char*);
}Actuator;


/* Importing certificates, key and endpoint */
extern char *ca_cert, *client_endpoint, *client_cert, *client_key;

/* Initializing environment variables */
static int wifi_alive = 0;
static int ssl_reset;
static SSLConnection *ssl_conn;
mqtt_client_t client = mqtt_client_default;
static QueueHandle_t publish_queue;
static Sensor sensors[NUMBER_OF_SENSORS];
static Actuator actuators[NUMBER_OF_ACTUATORS];
static uint8_t sensor_counter= 0;
static uint8_t actuator_counter = 0;
static uint8_t sense_busy = 0;
static SS_Stat simple_sense_status = simple_sense_deep_pause;
static uint8_t simple_sense_connected = 0;

/* Service pause,start controller */
void simple_sense_controller(int command){
    simple_sense_status = command;
}

/* Return simple_sense connection status */
uint8_t simple_sense_is_connected(){
    return simple_sense_connected;
}
/* Sensor register function */
void sensor_init(const char * name, uint8_t port, int (*callback)(uint8_t)){
    if(sensor_counter >= NUMBER_OF_SENSORS)
        printf("Warning: The program is trying to initialize more than %d sensors.\n", NUMBER_OF_SENSORS);
    else{
        gpio_enable(port, GPIO_INPUT);
        sensors[sensor_counter].name = name;
        sensors[sensor_counter].port = port;
        sensors[sensor_counter].callback = callback;
        sensor_counter ++;
    }
}

/* Actuator register function */
void actuator_init(char * name, void (*callback)(char*)){
    if(actuator_counter >= NUMBER_OF_ACTUATORS)
        printf("Warning: The program is trying to initialize more than %d actuators.\n", NUMBER_OF_ACTUATORS);
    else{
        strcat(actuators[actuator_counter].name,name);
        strcat(actuators[actuator_counter].name, "\":");
        actuators[actuator_counter].name_len = strlen(actuators[actuator_counter].name);
        actuators[actuator_counter].callback = callback;
        actuator_counter ++;
    }
}

int simple_publish(const char* msg, const char* topic){
    
    mqtt_message_t message;
    message.payload = msg;
    message.payloadlen = strlen(msg);
    message.dup = 0;
    message.qos = MQTT_QOS1;
    message.retained = 0;

    return mqtt_publish(&client, topic, &message);
}
/* Sense message publisher */
void sense(){
    char msg[PUBLISH_LENGHT];
    sense_busy = 1;
  
    msg[0] = '{';
    msg[1] = '\0';
    strcat(msg, "\n\"state\" : {\n\"reported\" : {");
    for(int i = 0; i < sensor_counter; i++){
        strcat(msg,"\"");
        strcat(msg, sensors[i].name);
        strcat(msg, "\":");

        char value[10];
        value[0] = '\0';
                
        if(sensors[i].callback == 0)    
            strcat(msg, itoa(gpio_read(sensors[i].port),value,10));
        else
            strcat(msg, itoa(sensors[i].callback(sensors[i].port),value,10));
        if(i != sensor_counter - 1)
            strcat(msg, ",\n");
    }
            
    strcat(msg, "}\n}\n}");
    printf("Publishing:\n%s\n",msg);

    mqtt_message_t message;
    message.payload = msg;
    message.payloadlen = strlen(msg);
    message.dup = 0;
    message.qos = MQTT_QOS1;
    message.retained = 0;

    while(mqtt_publish(&client, MQTT_PUB_TOPIC, &message) != MQTT_SUCCESS)
            vTaskDelay(1000 / portTICK_PERIOD_MS);

            
}

/* Sense clock task */
void sync(void *pvParameters) {
    char msg[16];

    while (1) {
        if(simple_sense_status != simple_sense_run){
            vTaskDelay(2000 / portTICK_PERIOD_MS);
            continue;
        }
        if(sense_busy){
            sense_busy = 0;
        }
        if (!wifi_alive) {
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }

        printf("%s: ready to publish.\r\n",__func__);

       if (xQueueSend(publish_queue, (void *) msg, 0) == pdFALSE) {
            printf("Publish queue overflow\r\n");
        }

        vTaskDelay(REFRESH_RATE / portTICK_PERIOD_MS);
    }
}

/*Actuators manager function*/
static void actuate(mqtt_message_data_t *md) {
    mqtt_message_t *message = md->message;
    char msg[PUBLISH_LENGHT];
    
    strcpy(msg,(char *)(message->payload));
    msg[(int) message->payloadlen] = '\0';

    //printf("Received:\n%s\n",msg);

    char* search;
    int i;
    int j;

    for(i = 0; i < actuator_counter; i ++){
        if(search = strstr(msg,actuators[i].name)){
            j = 0;
            char key_value[20];
            for(int k = search - msg + actuators[i].name_len; msg[k]!= '}' &&  msg[k]!=',' &&  msg[k]!='\n';k++,j++)
                key_value[j] = msg[k];
            key_value[j] = '\0';

            actuators[i].callback(key_value);
        }
    }
}


static const char *get_my_id(void) {
    // Use MAC address for Station as unique ID
    static char my_id[13];
    static bool my_id_done = false;
    int8_t i;
    uint8_t x;
    if (my_id_done)
        return my_id;
    if (!sdk_wifi_get_macaddr(STATION_IF, (uint8_t *) my_id))
        return NULL;
    for (i = 5; i >= 0; --i) {
        x = my_id[i] & 0x0F;
        if (x > 9)
            x += 7;
        my_id[i * 2 + 1] = x + '0';
        x = my_id[i] >> 4;
        if (x > 9)
            x += 7;
        my_id[i * 2] = x + '0';
    }
    my_id[12] = '\0';
    my_id_done = true;
    return my_id;
}

static int mqtt_ssl_read(mqtt_network_t * n, unsigned char* buffer, int len,
        int timeout_ms) {
    int r = ssl_read(ssl_conn, buffer, len, timeout_ms);
    if (r <= 0
            && (r != MBEDTLS_ERR_SSL_WANT_READ
                    && r != MBEDTLS_ERR_SSL_WANT_WRITE
                    && r != MBEDTLS_ERR_SSL_TIMEOUT)) {
        printf("%s: TLS read error (%d), resetting\n\r", __func__, r);
        ssl_reset = 1;
    };
    return r;
}

static int mqtt_ssl_write(mqtt_network_t* n, unsigned char* buffer, int len,
        int timeout_ms) {
    int r = ssl_write(ssl_conn, buffer, len, timeout_ms);
    if (r <= 0
            && (r != MBEDTLS_ERR_SSL_WANT_READ
                    && r != MBEDTLS_ERR_SSL_WANT_WRITE)) {
        printf("%s: TLS write error (%d), resetting\n\r", __func__, r);
        ssl_reset = 1;
    }
    return r;
}
/* Simple Sense service task */
void simple_sense(void *pvParameters) {
    int ret = 0;
    publish_queue = xQueueCreate(3, 16);
    struct mqtt_network network;
    char mqtt_client_id[20];
    uint8_t mqtt_buf[MQTT_BUFFER_SIZE];
    uint8_t mqtt_readbuf[MQTT_READBUFFER_SIZE];
    mqtt_packet_connect_data_t data = mqtt_packet_connect_data_initializer;

    memset(mqtt_client_id, 0, sizeof(mqtt_client_id));
    strcpy(mqtt_client_id, "ESP-");
    strcat(mqtt_client_id, get_my_id());

    ssl_conn = (SSLConnection *) malloc(sizeof(SSLConnection));
    while (1) {
        if (!wifi_alive || simple_sense_status == simple_sense_deep_pause) {
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            continue;
        }

        printf("%s: started\n\r", __func__);
        ssl_reset = 0;
        ssl_init(ssl_conn);
        ssl_conn->ca_cert_str = ca_cert;
        ssl_conn->client_cert_str = client_cert;
        ssl_conn->client_key_str = client_key;

        mqtt_network_new(&network);
        network.mqttread = mqtt_ssl_read;
        network.mqttwrite = mqtt_ssl_write;

        printf("%s: connecting to MQTT server %s ... ", __func__,
                client_endpoint);
        ret = ssl_connect(ssl_conn, client_endpoint, MQTT_PORT);

        if (ret) {
            printf("error: %d\n\r", ret);
            ssl_destroy(ssl_conn);
            continue;
        }
        printf("done\n\r");
        mqtt_client_new(&client, &network, 5000, mqtt_buf, MQTT_BUFFER_SIZE, mqtt_readbuf,
                MQTT_READBUFFER_SIZE);

        data.willFlag = 0;
        data.MQTTVersion = 4;
        data.cleansession = 1;
        data.clientID.cstring = mqtt_client_id;
        data.username.cstring = NULL;
        data.password.cstring = NULL;
        data.keepAliveInterval = 1000;
        printf("Send MQTT connect ... ");
        ret = mqtt_connect(&client, &data);
        if (ret) {
            printf("error: %d\n\r", ret);
            ssl_destroy(ssl_conn);
            continue;
        }
        printf("done\r\n");
        mqtt_subscribe(&client, MQTT_SUB_TOPIC, MQTT_QOS1, actuate);
        simple_sense_connected = 1;
        {
            char shadow_get[100];
            snprintf(shadow_get, 100, "$aws/things/%s/shadow/get",AWS_IOT_THING_NAME);
            simple_publish("",shadow_get);
            printf("%s: getting device shadow...\n",__func__ );
        }
        xQueueReset(publish_queue);

        while (wifi_alive && !ssl_reset) {
            if(sensor_counter > 0 && simple_sense_status == simple_sense_run){
                char msg[64];
                while (!sense_busy && xQueueReceive(publish_queue, (void *) msg, 0) == pdTRUE) 
                    sense();
                }

            ret = mqtt_yield(&client, 1000);
            if (ret == MQTT_DISCONNECTED)
                break;
        }
        printf("Connection dropped, request restart\n\r");
        simple_sense_connected = 0;
        ssl_destroy(ssl_conn);
    }
}

/* Wifi connection*/
void wifi_task(void *pvParameters) {
    uint8_t status = 0;
    uint8_t retries = 30;
    struct sdk_station_config config = { .ssid = WIFI_SSID, .password =
            WIFI_PASS, };

    printf("%s: Connecting to WiFi\n\r", __func__);
    sdk_wifi_set_opmode (STATION_MODE);
    sdk_wifi_station_set_config(&config);

    while (1) {
        wifi_alive = 0;

        while ((status != STATION_GOT_IP) && (retries)) {
            status = sdk_wifi_station_get_connect_status();
            printf("%s: status = %d\n\r", __func__, status);
            if (status == STATION_WRONG_PASSWORD) {
                printf("WiFi: wrong password\n\r");
                break;
            } else if (status == STATION_NO_AP_FOUND) {
                printf("WiFi: AP not found\n\r");
                break;
            } else if (status == STATION_CONNECT_FAIL) {
                printf("WiFi: connection failed\r\n");
                break;
            }
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            --retries;
        }

        while ((status = sdk_wifi_station_get_connect_status())
                == STATION_GOT_IP) {
            if (wifi_alive == 0) {
                printf("WiFi: Connected\n\r");
                wifi_alive = 1;
            }
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }

        wifi_alive = 0;
        printf("WiFi: disconnected\n\r");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
